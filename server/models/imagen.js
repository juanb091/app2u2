var models = require('./models'),
Schema= models.Schema;

var imagenSchema = new Schema({
	titulo: String,
	imagen: String
});

var Imagen=models.model('Imagen',imagenSchema,'imagen_sesion');
module.exports=Imagen;