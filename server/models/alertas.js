var models=require('./models'),
Schema=models.Schema;

var alertasSchema= new Schema({
	descripcionAlerta:String,
	materia:String,
	materia_nombre:String,
	student:String,
	fecha:{type:Date, default:Date()}
});

var Alertas = models.model('Alerta',alertasSchema,'alertas');
module.exports=Alertas;