var models=require('./models'),
Schema=models.Schema;

var enlacesSchema= new Schema({
	Titulo:String,
	Enlace:String,
	Comentario:String,
	materia:String,
	materia_nombre:String,
	student:String,
	fecha:{type:Date, default:Date()}
});

var Enlaces = models.model('Enlace',enlacesSchema,'enlaces');
module.exports=Enlaces;