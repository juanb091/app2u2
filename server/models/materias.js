var models = require('./models'),
Schema = models.Schema;

var materiasSchema = new Schema({
	profesor: String,
	curso: String,
	semestre: String,
	dias: String,
	codigo: String,
	hora: String,
	students:[String],
	institucion:String
});

var Materias= models.model('Materia',materiasSchema,'materias');
module.exports=Materias;