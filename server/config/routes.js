var usuarios=require('../controllers/usuarios');
var tareas=require('../controllers/tareas');	
var passport=require('./passport');
var alertas=require('../controllers/alertas');
var enlaces=require('../controllers/enlaces');
var materias=require('../controllers/materias');
var imagen=require('../controllers/imagen');


module.exports = function(app){

	app.get('/partials/*', function(req, res) {
	  	res.render('../../public/app/' + req.params['0']);
	});

	//login y registro GET & POST
	app.post('/registro',usuarios.registro);
	app.post('/registro/imagen',imagen.saveImage);

	app.post('/login',usuarios.login);

	app.post('/logout',usuarios.logout);

	app.get('/session',usuarios.userAuthenticated);

	app.get('/auth/twitter',passport.authenticate('twitter'));

	app.get('/auth/twitter/callback',
		passport.authenticate('twitter',{
			successRedirect:'/',
			failureRedirect:'/login'
		}));

	/*app.get('/auth/twitter',passport.authenticate('twitter'));

	app.get('/auth/twitter/callback',
		passport.authenticate('twitter', 
			{ successRedirect: '/',
			failureRedirect: '/login'
			 }));
*/
	//Alertas GET & POST
	app.post('/tareas',tareas.guardar);

	app.get('/tareas',tareas.getTareas);

	app.post('/tareas/finalizadas',tareas.guardarFinalizadas);

	//Alertas GET & POST
	app.post('/alertas',alertas.guardar_alerta);

	app.get('/alertas',alertas.getAlertas);

	//Enlaces GET & POST
	app.post('/enlaces',enlaces.guardar_enlace);

	app.get('/enlaces',enlaces.getEnlaces);


	//Materias GET & POST
	app.post('/materias',materias.guardar);

	app.post('/materias_desinscribir',materias.desinscribir);

	app.get('/materias',materias.getMaterias);



	app.get('*', function(req, res) {
	  	res.render('index');
	});


};