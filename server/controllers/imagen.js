var Imagen = require('../models/imagen');
var Usuario = require('../models/usuarios');
var _ = require('lodash');
var path = require('path');
var fs = require('fs');

exports.saveImage = function(req, res, next){
	//res.send(req.files);
	var newImagen = new Imagen({
		titulo : req.body.titulo
	});
	console.log(req.files);
	newImagen.imagen = guardar_archivo(req, res, req.files.file, newImagen);
	newImagen.save(function(err, imagen){
		if (!err) {
			next();
		}else{
			res.send({success : false, mensaje : err});
		}
	});
};

function guardar_archivo(req, res, file, obj){
	var root = path.dirname(require.main.filename);
	var originalFilename = file.originalname.split('.');
	var ext = originalFilename[originalFilename.length - 1];
	var nombre_archivo = req.session.passport.user.nombre+obj.titulo.toString()+'_'+obj._id.toString()+'.'+ext;
	
	var newPath = root + '/public/img/profiles/'+nombre_archivo.replace(/ /g,'');
	var newFile = new fs.createWriteStream(newPath);
	var oldFile = new fs.createReadStream(file.path);
	var bytes_totales = req.headers['content-length'];
	var bytes_subidos = 0;

	oldFile.pipe(newFile);

	oldFile.on('data', function (chunk){
		bytes_subidos += chunk.length;
		var progreso = (bytes_subidos / bytes_totales) * 100;
		//console.log("progress: "+parseInt(progreso, 10) + '%\n');
		console.log("progress: "+parseInt(progreso, 10) + '%\n');
	});

	oldFile.on('end', function(){
		console.log('Carga completa!');
	});
	return nombre_archivo.replace(/ /g,'');
}

