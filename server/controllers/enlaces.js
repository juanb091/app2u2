var Enlaces= require('../models/enlaces');
var ObjectId= require('mongoose').Types.ObjectId;
var _ = require('lodash');

exports.guardar_enlace=function(req,res,next){
	var student=ObjectId(req.session.passport.user._id.toString()).toString();
	var enlaces = new Enlaces({
		Titulo:req.body.tituloEnlace,
		Enlace:req.body.enlace,
		Comentario:req.body.comentarioEnlace,
		student:student,
		materia:req.body.materia,
		materia_nombre:req.body.materia_nombre
	});

	enlaces.save(function(err,enlace){
		if(err){
			console.log("err: "+err);
			res.send({success:false, message:err});
		}else{
			console.log("Enlace guardado con exito!");
			res.send({success: true, enlace:enlace});
		}
	});
};

exports.getEnlaces=function(req,res,next){
	console.log("pepepe");
	Enlaces.find({})
	.exec(function(err,enlaces){
		if(err){
			console.log(err);
			//console.log("hahhaha");
		}else{
			//console.log("hahahaha");
			res.send(enlaces);
		}
	});
};