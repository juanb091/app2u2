angular.module('MainApp').controller('alertasCtrl',function($scope,AlertasService,$stateParams,$filter,$rootScope,MateriasService, EnlacesService){
	
	var checkeados=[];
	var materia_id=$stateParams.id_materia;
	var materia_f="";
	$scope.enlaces_materia=[];
	$scope.alertas_materia=[];
	
	$rootScope.alertas=[];
	
	$scope.item_master_alertas={descripcionAlerta:""};
	$scope.item_master_enlaces={tituloEnlace:"",comentarioEnlace:"",enlace:""};
	$scope.date = new Date();

	$scope.guardar_alerta=function(){
		MateriasService.getAsignatura()
		.then(function(response){
			//console.log(materia_id);
			materia_f=$filter('filter')(response.data, {_id:materia_id}, true);
			//console.log(materia_f);
			materia_f=materia_f[0].curso;
			//console.log(materia_f);

			AlertasService.guardarAlertas({descripcionAlerta:$scope.alerta.descripcionAlerta,materia:materia_id,materia_nombre:materia_f})
			.then(function(response){
				if(response.data.success){
					$scope.alertas_materia.push(response.data.alerta);
					$rootScope.alertas.push(response.data.alerta);
					angular.copy($scope.item_master_alertas,$scope.alerta);
				}
			});
		});	
	}

	AlertasService.getAlertas()
	.then(function(response){
		f_alertas=$filter('filter')(response.data, {materia:materia_id}, true);
		_.each(f_alertas, function(item){
				//console.log(item);

				$scope.alertas_materia.push(item);
			
		});
	});


	// Enlaces control ---------------------------------------------------------


	$scope.guardar_enlace=function(){
		MateriasService.getAsignatura()
		.then(function(response){
			//console.log(materia_id);
			materia_f=$filter('filter')(response.data, {_id:materia_id}, true);
			//console.log(materia_f);
			materia_f=materia_f[0].curso;
			//console.log(materia_f);
			var enlace_www_pos=$scope.enlace.enlace.search("www");
			var enlace_www=$scope.enlace.enlace.substr(enlace_www_pos,$scope.enlace.enlace.length-1);
			console.log(enlace_www);
			EnlacesService.guardarEnlaces({
				tituloEnlace:$scope.enlace.tituloEnlace, 
				comentarioEnlace:$scope.enlace.comentarioEnlace,
				enlace:enlace_www,
				materia:materia_id,
				materia_nombre:materia_f})
			.then(function(response){
				if(response.data.success){
					$scope.enlaces_materia.push(response.data.enlace);
					
					angular.copy($scope.item_master_enlaces,$scope.enlace);
				}
			});
		});	
	}

	EnlacesService.getEnlaces()
	.then(function(response){
		f_enlaces=$filter('filter')(response.data, {materia:materia_id}, true);
		_.each(f_enlaces, function(item){
				console.log(item);

				$scope.enlaces_materia.push(item);
				console.log($scope.enlaces_materia);
			
		});
	});
	
});