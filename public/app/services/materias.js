angular.module('MainApp').factory('MateriasService',function($http){
	return{
		
		getAsignatura:function(){
			//console.log("getAsignatura");
			return $http.get('/materias');
		},
		
		guardarMateria:function(datos){
			//console.log("guardarMateria");
			return $http.post('/materias',datos);
		},
		Desinscribir:function(materia){
			//console.log("guardarMateria");
			console.log(materia);
			return $http.post('/materias_desinscribir',materia);
		}
	}
});