var app = angular.module('MainApp',['ui.router', 'ngAnimate', 'toastr','ui.bootstrap','datatables', 'ngResource','angular.filter']);

app.config(['$stateProvider',"$urlRouterProvider", function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/app/dashboard');

	$stateProvider
		.state('app',{
			url : '/app',
			templateUrl : 'partials/index/templates/index.html',
			controller : 'indexCtrl'
		})
		.state('app.dashboard',{
			url : '/dashboard',
			templateUrl : 'partials/dashboard/templates/dashboard.html',
			controller : 'dashboardCtrl'
		})
		.state('app.tareas',{
			url : '/tareas',
			templateUrl : 'partials/tareas/templates/tareas.html',
			controller:'tareasCtrl'
		})
		.state('registro',{
			url : '/registro',
			templateUrl : 'partials/sign/templates/registro.html',
			controller : 'registroCtrl'
		})
		.state('app.perfil',{
			url : '/perfil',
			templateUrl : 'partials/perfil/templates/perfil.html',
		})
		.state('info',{
			url : '/informacion',
			templateUrl : 'partials/informacion/templates/info.html',
		})
		.state('login',{
			url : '/login',
			templateUrl : 'partials/sign/templates/login.html',
			controller : 'loginCtrl'
		})
		.state('app.calendario',{
			url:'/calendario',
			templateUrl:'partials/calendario/templates/calendario.html'
		})
		.state('app.materia',{
			url:'/materia/:id_materia',
			templateUrl:'partials/materia/templates/materia.html',
			controller:'alertasCtrl'
		})
		.state('app.inscribir',{
			url:'/inscribir',
			templateUrl:'partials/inscribir/templates/inscribir.html',
			controller:'inscribirCtrl'
		})
}]);