var app = angular.module('MainApp');

app.controller('indexCtrl', function($rootScope,$state, $scope, Session,MateriasService){

	function Modulo(state){

		this.state = state.name;

		this.name = state.name.split('.')[1];

		this.getName = function(){
			return this.name && this.name[0].toUpperCase() + this.name.slice(1);
		};
	}
	

	/////////*********Scopes********//////////
	
	$scope.modulo = new Modulo($state.current).getName();
	$rootScope.num_materias=0;
	$rootScope.materias=[];
	$rootScope.materias_inscritas=[];
	$rootScope.num_mat=0;
	var user_id="";
	Session.getUsuario()
	.then(function(response){
		//console.log(response.data.user.user);
		user_id=response.data.user.user;
		//console.log(user_id);
		MateriasService.getAsignatura()
		.then(function(response){
		_.each(response.data, function(item){
				//console.log(item);
				$rootScope.materias.push(item);
				//console.log(user_id);

				if(user_id && item.students.indexOf(user_id._id)>-1){
					//console.log("cargando a inscritas");
					$rootScope.materias_inscritas.push(item);
					$rootScope.num_mat=$rootScope.materias_inscritas.length;
				};
				
			});
		});
	});

	$scope.eliminar_curso=function(materia){
		 var index = $rootScope.materias_inscritas.indexOf(materia);
  		$rootScope.materias_inscritas.splice(index, 1);
  		MateriasService.Desinscribir(materia);
  		$rootScope.num_mat=$rootScope.num_mat-1;  
	}

	$scope.goToMateria=function(destino){
		$state.go('app.materia',{id_materia:destino});
	};
	
	$scope.logout = function(){
		Session.logOut()
		.then(function(response){
			if (response.data.destroy) {
				$state.go('login');
			}
		});
	}

	Session.getUsuario()
	.then(function(response){
		$scope.usuario = response.data.user.user;
	});

	Session.isLogged()
	.then(function(response){
		var isLogged = response.data.isLogged;
		if (!isLogged) {
			$state.go('login');
		}

	});


	//Events
	$rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
		
		$scope.modulo = new Modulo(toState).getName();
	});
});	



