angular.module('MainApp').controller('inscribirCtrl',function($scope,$rootScope,MateriasService,$uibModal, $log,Session,$state,toastF){

	$rootScope.materias=[];
	var user=null;
	$scope.checked=false;
	$scope.checkboxModel = {
       		lunes : false,
       		martes: false,
       		miercoles: false,
       		jueves: false,
       		viernes: false,
       		sabado: false
     	};

	$scope.item_master={profesor:"",curso:"",semestre:"",codigo:"",hora:""};

	$scope.onFieldChange = function () {
					 	if ($scope.checked == false){
                            if ($scope.checkboxModel.lunes == true) {          
                                $scope.isRequiredlunes = true;
                            };
                            if ($scope.checkboxModel.martes == true) {          
                                $scope.isRequiredmartes = true;
                            };
                            if ($scope.checkboxModel.miercoles == true) {          
                                $scope.isRequiredmiercoles = true;
                                console.log("miercoles");
                            };
                            if ($scope.checkboxModel.jueves == true) {          
                                $scope.isRequiredjueves = true;
                                console.log("Jueves");
                            };
                            if ($scope.checkboxModel.viernes == true) {          
                                $scope.isRequiredviernes = true;
                                console.log("Viernes");
                            };
                            if ($scope.checkboxModel.sabado == true) {          
                                $scope.isRequiredsabado = true;
                                console.log("Sabado");
                            };
                        }
                        else{
                            	$scope.isRequiredlunes = false;
                            	$scope.isRequiredmartes = false;
                            	$scope.isRequiredmiercoles = false;
                            	$scope.isRequiredjueves = false;
                            	$scope.isRequiredviernes = false;
                                $scope.isRequiredsabado = false;
                            }
                   }

	$scope.guardar=function(){

		var dias=[];
		var hora="";


		if($scope.checked == true){
				var tim=($("#timepicker6").attr("data-timepicki-tim")).toString()+":";
				var min=($("#timepicker6").attr("data-timepicki-mini")).toString()+":";
				var meri=($("#timepicker6").attr("data-timepicki-meri")).toString();
				var hora=tim.concat(min.concat(meri));

				if ($scope.checkboxModel.lunes) {dias.push("L")};
		     	if ($scope.checkboxModel.martes) {dias.push("Ma")};
		     	if ($scope.checkboxModel.miercoles) {dias.push("Mi")};
		     	if ($scope.checkboxModel.jueves) {dias.push("J")};
		     	if ($scope.checkboxModel.viernes) {dias.push("V")};
		     	if ($scope.checkboxModel.sabado) {dias.push("S")};
		     	dias=dias.join(',');
		}
		else if($scope.checked == false){

				if ($scope.checkboxModel.lunes) {
					dias.push("L")
					var timlunes=($("#horalunes").attr("data-timepicki-tim")).toString()+":";
					var minlunes=($("#horalunes").attr("data-timepicki-mini")).toString()+":";
					var merilunes=($("#horalunes").attr("data-timepicki-meri")).toString();
					var horalunes=" L: "+(timlunes.concat(minlunes.concat(merilunes)))+"; ";
					hora+=horalunes;
				};
		     	if ($scope.checkboxModel.martes) {
		     		dias.push("Ma")
		     		var timmartes=($("#horamartes").attr("data-timepicki-tim")).toString()+":";
					var minmartes=($("#horamartes").attr("data-timepicki-mini")).toString()+":";
					var merimartes=($("#horamartes").attr("data-timepicki-meri")).toString();
					var horamartes=" Ma: "+(timmartes.concat(minmartes.concat(merimartes)))+"; ";
					hora+=horamartes;
		     	};
		     	if ($scope.checkboxModel.miercoles) {
		     		dias.push("Mi")
		     		var timmiercoles=($("#horamiercoles").attr("data-timepicki-tim")).toString()+":";
					var minmiercoles=($("#horamiercoles").attr("data-timepicki-mini")).toString()+":";
					var merimiercoles=($("#horamiercoles").attr("data-timepicki-meri")).toString();
					var horamiercoles=" Mi: "+(timmiercoles.concat(minmiercoles.concat(merimiercoles)))+"; ";
					hora+=horamiercoles;
		     	};
		     	if ($scope.checkboxModel.jueves) {
		     		dias.push("J")
		     		var timjueves=($("#horajueves").attr("data-timepicki-tim")).toString()+":";
					var minjueves=($("#horajueves").attr("data-timepicki-mini")).toString()+":";
					var merijueves=($("#horajueves").attr("data-timepicki-meri")).toString();
					var horajueves=" J: "+(timjueves.concat(minjueves.concat(merijueves)))+"; ";
					hora+=horajueves;
		     	};
		     	if ($scope.checkboxModel.viernes) {
		     		dias.push("V")
		     		var timviernes=($("#horaviernes").attr("data-timepicki-tim")).toString()+":";
					var minviernes=($("#horaviernes").attr("data-timepicki-mini")).toString()+":";
					var meriviernes=($("#horaviernes").attr("data-timepicki-meri")).toString();
					var horaviernes=" V: "+(timviernes.concat(minviernes.concat(meriviernes)))+"; ";
					hora+=horaviernes;
		     	};
		     	if ($scope.checkboxModel.sabado) {
		     		dias.push("S")
		     		var timsabado=($("#horasabado").attr("data-timepicki-tim")).toString()+":";
					var minsabado=($("#horasabado").attr("data-timepicki-mini")).toString()+":";
					var merisabado=($("#horasabado").attr("data-timepicki-meri")).toString();
					var horasabado=" S: "+(timsabado.concat(minsabado.concat(merisabado)))+"; ";
					hora+=horasabado;
		     	};
		     	dias=dias.join(',');
		}

     	
 
		$scope.item.horalunes="";
		$scope.item.horamartes="";
		$scope.item.horamiercoles="";
		$scope.item.horajueves="";
		$scope.item.horaviernes="";
		$scope.item.horasabado="";

     	$scope.checkboxModel = {
       		lunes : false,
       		martes: false,
       		miercoles: false,
       		jueves: false,
       		viernes: false,
       		sabado: false
     	};
     	console.log(user);
		MateriasService.guardarMateria({
			profesor:$scope.item.profesor,
			curso:$scope.item.curso,
			semestre:$scope.item.semestre,
			codigo:$scope.item.codigo,
			hora:hora,
			dias:dias,
			institucion:user.institucion})
		.then(function(response){
			if(response.data.success && !response.data.exists){
				toastF.success('Creaste la materia correctamente!');		
				$rootScope.materias.push(response.data.materia);
				//$state.go($state.current, {}, {reload: true});
				angular.copy($scope.item_master,$scope.item);
			}else if(response.data.success && response.data.exists){
				//console.log("la materia ya existe");
				open();			
			}
		});
	}

	Session.getUsuario()
	.then(function(response){
		//console.log(response.data.user.user);
		user=response.data.user.user;
	});
	
	MateriasService.getAsignatura()
	.then(function(response){
		//$rootScope.num_materias=response.data.length;
		$rootScope.materias=[];
		$rootScope.materias_inscritas=[];
		_.each(response.data, function(item){
				//console.log(item);
				//$rootScope.materias_inscritas.push(item);
				if(user!=null && item.institucion==user.institucion){
					$rootScope.materias.push(item);
				};
				
				if(user!=null && item.students.indexOf(user._id)>-1){
					//console.log("cargando a inscritas");
					$rootScope.materias_inscritas.push(item);
					$rootScope.num_mat=$rootScope.materias_inscritas.length;
				};
				if (user==null) {
					$state.go('app.dashboard');
				};
		});
	});


	//Modal Controller

	$scope.animationsEnabled = true;

	var open = function () {
	//console.log(open)
	var modalInstance = $uibModal.open({
		animation: $scope.animationsEnabled,
		templateUrl: 'myModalContent.html',
		controller: 'ModalInstanceCtrl'
		});
	};

	//checkboxes
	$scope.selected = [];
	$scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
          list.splice(idx, 1);
        }
        else {
          list.push(item);
          //console.log(list);
          //console.log($scope.selected);
        }
      };

    $scope.enroll = function (item, list) {
      return list.indexOf(item) > -1;
    };

    //inscribir checked

    $scope.inscribir=function(){
    	_.each($scope.selected,function(item){
    		MateriasService.guardarMateria(item)
    		.then(function(response){
    			if(response.data.success && response.data.exists && response.data.student_exists){
					//console.log("materia ya estaba inscrita")
				}else if(response.data.success && response.data.exists && !response.data.student_exists){
					//console.log(response.data.materia);
					$rootScope.materias_inscritas.push(response.data.materia);
					//console.log($rootScope.materias_inscritas);
					$rootScope.num_mat=$rootScope.materias_inscritas.length;
					//console.log("la materia ya está inscrita");
					//open();
					toastF.success('Inscribiste la materia correctamente!');			
				}
    		})
    	})
    }

     $scope.sortType     = 'name'; // set the default sort type
  $scope.sortReverse  = false;  // set the default sort order
  $scope.searchCourse   = '';     // set the default search/filter term
  
 


  //table pagination

   
    
});